Drupal quiz_reward module
========================

What Is This?
-------------
This is a module for Drupal (http://drupal.org) release 7 that supplements the
Quiz module (http://drupal.org/project/quiz) by emailing a reward code to a user
that passes a quiz.  This version assumes that there is only one quiz available
so any user that passes any quiz on the site receives the reward code.  This
module was originally written for the Utah Avalanche Center
(http://utahavalanchecenter.org) so they could offer the public an instructional
video followed by a quiz, with anyone who passed the quiz receiving a code
good for a discount at local merchants.